import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './../screens/home/Home';

const Stack = createStackNavigator();
Stack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

function HomeStack() {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false
      }}>
      <Stack.Screen
        name="Home"
        component={Home}/>
    </Stack.Navigator>
  );
}

export default HomeStack;