import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, ScrollView, TouchableOpacity, StatusBar, RefreshControl } from 'react-native';
import { BrandColor } from './../../components/Color';
import { EventText, EventTextBold } from './../../components/Theme';
import axios from 'axios';
import moment from 'moment';
import 'moment/locale/id';
import AsyncStorage from '@react-native-community/async-storage';
import { Button } from 'react-native-elements';

const Home = ({ navigation  }) => {

	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(true);
	const [modal, setModal] = useState(false);

	const getDataService = (modal=false) =>{
		axios.get('https://api.themoviedb.org/3/discover/movie?api_key=f7b67d9afdb3c971d4419fa4cb667fbf')
		.then(response => {
			setData(response.data.results);
			AsyncStorage.setItem('data', JSON.stringify(response.data.results));
			setLoading(false);
			if(modal){
				setModal(true);
				setTimeout(function() {
					setModal(false);
				}, 1000*5)
			}
		})
		.catch(error => {
			setLoading(false);
			console.log('Response error ', error.config);
		});
	}

	const getData = async () =>{
		var data = await AsyncStorage.getItem('data');
		if(data){
			var dataJson = JSON.parse(data);
			setData(dataJson);
			setLoading(false);
		}else{
			getDataService();
		}
	}

	useEffect(()=>{
		getData();

		setInterval(function() {
			getDataService(true);
		}, 1000*60);
	}, [])

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor:'#eeeff3' }}>
      <StatusBar backgroundColor={'#000'} barStyle="light-content"/>

			<View style={{padding:15, marginTop:15, flexDirection:'row', alignItems:'center'}}>
				<EventTextBold style={{fontSize:18, color: '#000', flex: 1}}>Film Popular</EventTextBold>
				<TouchableOpacity><EventTextBold style={{color:BrandColor()}}>Lihat Semua</EventTextBold></TouchableOpacity>
			</View>

			<ScrollView 
				showsVerticalScrollIndicator={false}
				refreshControl={
          <RefreshControl refreshing={loading} onRefresh={()=>getDataService()} />
        }
			>
				{loading?<View/>:renderContent()}
			</ScrollView>

			{
				modal?
				<View style={{ backgroundColor: 'white', padding: 15, borderColor: 'rgba(0, 0, 0, 0.1)', borderTopWidth:1, elevation:4}}>
					<View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
          	<EventText style={{flex:1, fontSize:16}}>Penyimpanan lokal telah diperbaharui</EventText>
						<View style={{width:100}}>
							<Button
								title="Tampilkan"
								onPress={()=>{
									setModal(false);
									setLoading(true);
									getData();
								}}
							/>
						</View>
					</View>
        </View>:<View/>
			}
    </SafeAreaView>
	);

	function renderContent() {
		return(
			<View style={{paddingHorizontal:15}}>
				{
					data.map((item, index)=>{
						return(
							<TouchableOpacity style={{backgroundColor:'#fff', borderRadius:10, overflow: 'hidden', marginBottom:15}} key={'data-list-'+index.toString()}>
								<View style={{padding:15}}>
									<EventTextBold style={{fontSize:18}}>{item.original_title}</EventTextBold>
									<EventTextBold style={{color:'#ff5722', marginTop:6}}>Rilis: {moment(item.release_date).format('DD MMMM YYYY')}</EventTextBold>
								</View>
							</TouchableOpacity>
						)
					})
				}
			</View>
		)
	}
}

export default Home;