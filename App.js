/**
 * @author Oman Fathurohman <oman21.dev@gmail.com>
 */

import React, { useEffect } from 'react';
import { ToastAndroid } from "react-native";
import { NavigationContainer } from '@react-navigation/native';;
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from 'react-native-splash-screen';
import NetInfo from "@react-native-community/netinfo";

import HomeStack from './src/routes/HomeStack';

const Stack = createStackNavigator();
Stack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

const App = () => {
  const CheckConnectivity = () => {
    NetInfo.fetch().then(state => {
      if(!state.isConnected) {
        ToastAndroid.showWithGravity(
          "Tidak ada koneksi internet",
          ToastAndroid.LONG,
          ToastAndroid.CENTER
        );
      }

      SplashScreen.hide();
    });
  };

  useEffect(() => {
    CheckConnectivity();
  }, [] );

  return(
    <NavigationContainer>
      <Stack.Navigator
      initialRouteName={HomeStack}
      screenOptions={{
        headerShown: false
      }}>
        <Stack.Screen
          name="HomeStack"
          component={HomeStack}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App
